.SUFFIXES: .sh 
T=linuxcc-prepare
all: $T
%: %.sh
	cp $< $@
	shar -t arch tools >> $@
clean:
	rm -f $T
